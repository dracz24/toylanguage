# Toy Language Project
# CISC 3160
# Daniel Racz

import string


# Error class

class Error:
    def __init__(self, pos_start, pos_end, error_name, details):
        self.pos_start = pos_start
        self.pos_end = pos_end
        self.error_name = error_name
        self.details = details

    def as_string(self):
        result = f'{self.error_name}: {self.details}\n'
        result += f'File {self.pos_start.fn}, line {self.pos_start.ln + 1}'
        return result

# Class to display errors if unknown character is entered
class IllegalCharError(Error):
    def __init__(self, pos_start, pos_end, details):
        super().__init__(pos_start, pos_end, 'Invalid Character', details)

# Class to display error during parsing process
class InvalidSyntax(Error):
    def __init__(self, pos_start, pos_end, details=''):
        super().__init__(pos_start, pos_end, 'Invalid Syntax', details)

# Class to detect division error if dividing by 0
class DivideError(Error):
    def __init__(self, pos_start, pos_end, details):
        super().__init__(pos_start, pos_end, 'Divide Error', details)
        self.details = details

    def as_string(self):
        result = self.generate_traceback()
        result += f'{self.error_name}: {self.details}'
        return result

    def generate_traceback(self):
        result = ''
        pos = self.pos_start
        ctx = self.details

        while ctx:
            result = f'  File {pos.fn}, line {str(pos.ln + 1)}, in {ctx.display_name}\n' + result
            pos = ctx.parent_entry_pos
            ctx = ctx.parent

        return 'Traceback (most recent call last):\n' + result



# Class to detect current position of input and advancing to next


class Position:
    def __init__(self, idx, ln, col, fn, ftxt):
        self.idx = idx
        self.ln = ln
        self.col = col
        self.fn = fn
        self.ftxt = ftxt

    def advance(self, current_char=None):
        self.idx += 1
        self.col += 1

        if current_char == '\n':
            self.ln += 1
            self.col = 0

        return self

# Copies position
    def copy(self):
        return Position(self.idx, self.ln, self.col, self.fn, self.ftxt)



# TOKENS

#Valid Entries
DIGITS = '0123456789'
ALPHABET = string.ascii_letters
ALPHA_DIGITS = ALPHABET + DIGITS

VAR_INT = 'INT'
VAR_FLOAT = 'FLOAT'
VAR_PLUS = 'PLUS'
VAR_MINUS = 'MINUS'
VAR_MUL = 'MULTIPLY'
VAR_DIV = 'DIVIDE'
VAR_POW = "POWER"
VAR_EQUAL = 'EQUAL'
LHS_PAR = 'LHSPAR'
RHS_PAR = 'RPAREN'
VAR_ID = 'Variable ID'
VAR_KEY = 'Key'
VAR_EOF = 'EOF'

#Variable keywords to input
KEYWORDS = [
    'int'
]

# Stored variable class to keep  track of variables and values associated with them


class StoredVar:
    def __init__(self):
        self.symbols = {}
        self.parent = None

# Attain value from variable
    def get(self, name):
        value = self.symbols.get(name, None)
        if value == None and self.parent:
            return self.parent.get(name)
        return value

    def set(self, name, value):
        self.symbols[name] = value

    def remove(self, name):
        del self.symbols[name]

# Details class to display trace on error from function or program


class Details:
    def __init__(self, display_name, parent=None, parent_entry_pos=None):
        self.display_name = display_name
        self.parent = parent
        self.parent_entry_pos = parent_entry_pos
        self.symbol_table = None


class Token:
    def __init__(self, type_, value=None, pos_start=None, pos_end=None):
        self.type = type_
        self.value = value

        if pos_start:
            self.pos_start = pos_start.copy()
            self.pos_end = pos_start.copy()
            self.pos_end.advance()

        if pos_end:
            self.pos_end = pos_end.copy()

    def matches(self, type_, value):
        return self.type == type_ and self.value == value

    def __repr__(self):
        if self.value: return f'{self.type}:{self.value}'
        return f'{self.type}'



# LEXER


class Lexer:
    def __init__(self, fn, text):
        self.fn = fn
        self.text = text
        self.pos = Position(-1, 0, -1, fn, text)
        self.current_char = None
        self.advance()


    def advance(self):
        self.pos.advance(self.current_char)
        self.current_char = self.text[self.pos.idx] if self.pos.idx < len(self.text) else None

# Will check character input and tokenize accordingly
    def tokenizer(self):
        tokens = []
        zero_count = 0


        while self.current_char != None:
            if self.current_char in ' \t':
                self.advance()
            elif self.pos == 1 and self.current_char == '0':
                return [], IllegalCharError(pos_start, self.pos, "'" + char + "'")
            elif self.current_char in DIGITS:
                tokens.append(self.make_number())
            elif self.current_char in ALPHABET:
                tokens.append(self.make_identifier())
            elif self.current_char == '+':
                tokens.append(Token(VAR_PLUS, pos_start=self.pos))
                self.advance()
            elif self.current_char == '-':
                tokens.append(Token(VAR_MINUS, pos_start=self.pos))
                self.advance()
            elif self.current_char == '*':
                tokens.append(Token(VAR_MUL, pos_start=self.pos))
                self.advance()
            elif self.current_char == '/':
                tokens.append(Token(VAR_DIV, pos_start=self.pos))
                self.advance()
            elif self.current_char == '^':
                tokens.append(Token(VAR_POW, pos_start=self.pos))
                self.advance()
            elif self.current_char == '=':
                tokens.append(Token(VAR_EQUAL, pos_start=self.pos))
                self.advance()
            elif self.current_char == '(':
                tokens.append(Token(LHS_PAR, pos_start=self.pos))
                self.advance()
            elif self.current_char == ')':
                tokens.append(Token(RHS_PAR, pos_start=self.pos))
                self.advance()
            else:
                pos_start = self.pos.copy()
                char = self.current_char
                self.advance()
                return [], IllegalCharError(pos_start, self.pos, "'" + char + "'")

        tokens.append(Token(VAR_EOF, pos_start=self.pos))
        return tokens, None

# Will convert number to integer.  Give error if . is entered.  Float's not accepted.
    def make_number(self):
        num_str = ''
        dot_count = 0
        zero_count = 0
        pos_start = self.pos.copy()

        while self.current_char != None and self.current_char in DIGITS + '.':
            if self.current_char == '.':
                if dot_count == 1: break
                dot_count += 1
            num_str += self.current_char
            self.advance()

        if dot_count == 0 :
            return Token(VAR_INT, int(num_str), pos_start, self.pos)
        else:
            return [], IllegalCharError(pos_start, self.pos, "'" + char + "'")

    def make_identifier(self):
        id_str = ''
        pos_start = self.pos.copy()

        while self.current_char != None and self.current_char in ALPHA_DIGITS + '_':
            id_str += self.current_char
            self.advance()

        tok_type = VAR_KEY if id_str in KEYWORDS else VAR_ID
        return Token(tok_type, id_str, pos_start, self.pos)



# Node classes to process specific tokens

#Process integer token
class NumberNode:
    def __init__(self, tok):
        self.tok = tok

        self.pos_start = self.tok.pos_start
        self.pos_end = self.tok.pos_end

    def __repr__(self):
        return f'{self.tok}'


class AccessV:
    def __init__(self, var_name_tok):
        self.var_name_tok = var_name_tok

        self.pos_start = self.var_name_tok.pos_start
        self.pos_end = self.var_name_tok.pos_end


class AssignV:
    def __init__(self, var_name_tok, value_node):
        self.var_name_tok = var_name_tok
        self.value_node = value_node

        self.pos_start = self.var_name_tok.pos_start
        self.pos_end = self.value_node.pos_end

# Process add, sub, mult and divide functions
class BinOpNode:
    def __init__(self, left_node, op_tok, right_node):
        self.left_node = left_node
        self.op_tok = op_tok
        self.right_node = right_node

        self.pos_start = self.left_node.pos_start
        self.pos_end = self.right_node.pos_end

    def __repr__(self):
        return f'({self.left_node}, {self.op_tok}, {self.right_node})'


class UnaryOpNode:
    def __init__(self, op_tok, node):
        self.op_tok = op_tok
        self.node = node

        self.pos_start = self.op_tok.pos_start
        self.pos_end = node.pos_end

    def __repr__(self):
        return f'({self.op_tok}, {self.node})'


# Class to check for errors in parsing process.

class ParseResult:
    def __init__(self):
        self.error = None
        self.node = None
        self.advance_count = 0

    def register_advancement(self):
        self.advance_count += 1

    def register(self, res):
        self.advance_count += res.advance_count
        if res.error: self.error = res.error
        return res.node
# Takes node if successful
    def success(self, node):
        self.node = node
        return self
# Returns error if not successful
    def fail(self, error):
        if not self.error or self.advance_count == 0:
            self.error = error
        return self



# Parser class to build syntax tree with tokens submitted by Lexer


class Parser:
    def __init__(self, tokens):
        self.tokens = tokens
        self.tok_idx = -1
        self.advance()

    def advance(self, ):
        self.tok_idx += 1
        if self.tok_idx < len(self.tokens):
            self.current_tok = self.tokens[self.tok_idx]
        return self.current_tok

    def parse(self):
        res = self.expr()
        if not res.error and self.current_tok.type != VAR_EOF:
            return res.fail(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "Missing operators: +, -, *, / or ^"
            ))
        return res



    def atom(self):
        res = ParseResult()
        tok = self.current_tok

        if tok.type in (VAR_INT, VAR_FLOAT):
            res.register_advancement()
            self.advance()
            return res.success(NumberNode(tok))

        elif tok.type == VAR_ID:
            res.register_advancement()
            self.advance()
            return res.success(AccessV(tok))

        elif tok.type == LHS_PAR:
            res.register_advancement()
            self.advance()
            expr = res.register(self.expr())
            if res.error: return res
            if self.current_tok.type == RHS_PAR:
                res.register_advancement()
                self.advance()
                return res.success(expr)
            else:
                return res.fail(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "Expected ')'"
                ))

        return res.fail(InvalidSyntaxError(
            tok.pos_start, tok.pos_end,
            "Expected int, float, identifier, '+', '-' or '('"
        ))

    def power(self):
        return self.bin_op(self.atom, (VAR_POW,), self.factor)

# Function looks for integer or float and returns number node of token
    def factor(self):
        res = ParseResult()
        tok = self.current_tok

        if tok.type in (VAR_PLUS, VAR_MINUS):
            res.register_advancement()
            self.advance()
            factor = res.register(self.factor())
            if res.error: return res
            return res.success(UnaryOpNode(tok, factor))

        return self.power()

# Function for multiply and division operations
    def term(self):
        return self.bin_op(self.factor, (VAR_MUL, VAR_DIV))
# Function for add and subtraction operations
    def expr(self):
        res = ParseResult()

        if self.current_tok.matches(VAR_KEY, 'int') or self.current_tok.matches(VAR_KEY, 'float') :
            res.register_advancement()
            self.advance()


            if self.current_tok.type != VAR_ID:
                return res.fail(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "Expected identifier"
                ))

            var_name = self.current_tok
            res.register_advancement()
            self.advance()

            if self.current_tok.type != VAR_EQUAL:
                return res.fail(InvalidSyntaxError(
                    self.current_tok.pos_start, self.current_tok.pos_end,
                    "Expected '='"
                ))

            res.register_advancement()
            self.advance()
            expr = res.register(self.expr())
            if res.error: return res
            return res.success(AssignV(var_name, expr))

        node = res.register(self.bin_op(self.term, (VAR_PLUS, VAR_MINUS)))

        if res.error:
            return res.fail(InvalidSyntaxError(
                self.current_tok.pos_start, self.current_tok.pos_end,
                "Expected 'int', int, float, identifier, '+', '-' or '('"
            ))

        return res.success(node)

    ###################################

    def bin_op(self, func_a, ops, func_b=None):
        if func_b == None:
            func_b = func_a

        res = ParseResult()
        left = res.register(func_a())
        if res.error: return res

        while self.current_tok.type in ops:
            op_tok = self.current_tok
            res.register_advancement()
            self.advance()
            right = res.register(func_b())
            if res.error: return res
            left = BinOpNode(left, op_tok, right)

        return res.success(left)


# Runtime resul that includes DivideError class for divide by zero cases


class DivideError:
    def __init__(self):
        self.value = None
        self.error = None

    def register(self, res):
        if res.error: self.error = res.error
        return res.value

    def success(self, value):
        self.value = value
        return self

    def fail(self, error):
        self.error = error
        return self



# VALUES


class Number:
    def __init__(self, value):
        self.value = value
        self.set_pos()
        self.set_details()

    def set_pos(self, pos_start=None, pos_end=None):
        self.pos_start = pos_start
        self.pos_end = pos_end
        return self

    def set_details(self, details=None):
        self.details = details
        return self

    def added_to(self, other):
        if isinstance(other, Number):
            return Number(self.value + other.value).set_details(self.details), None

    def subbed_by(self, other):
        if isinstance(other, Number):
            return Number(self.value - other.value).set_details(self.details), None

    def multed_by(self, other):
        if isinstance(other, Number):
            return Number(self.value * other.value).set_details(self.details), None

    def dived_by(self, other):
        if isinstance(other, Number):
            if other.value == 0:
                return None, RTError(
                    other.pos_start, other.pos_end,
                    'Division by zero',
                    self.details
                )

            return Number(self.value / other.value).set_details(self.details), None

    def powed_by(self, other):
        if isinstance(other, Number):
            return Number(self.value ** other.value).set_details(self.details), None

    def copy(self):
        copy = Number(self.value)
        copy.set_pos(self.pos_start, self.pos_end)
        copy.set_details(self.details)
        return copy

    def __repr__(self):
        return str(self.value)

# Class to interpret the node types and will execute correct procedures


class Interpreter:
    def visit(self, node, details):
        method_name = f'visit_{type(node).__name__}'
        method = getattr(self, method_name, self.no_visit_method)
        return method(node, details)

    def no_visit_method(self, node, details):
        raise Exception(f'No visit_{type(node).__name__} method defined')

    ###################################

    def visit_NumberNode(self, node, details):
        return DivideError().success(
            Number(node.tok.value).set_details(details).set_pos(node.pos_start, node.pos_end)
        )

    def visit_AccessV(self, node, details):
        res = DivideError()
        var_name = node.var_name_tok.value
        value = details.symbol_table.get(var_name)

        if not value:
            return res.fail(DivideError(
                node.pos_start, node.pos_end,
                f"'{var_name}' is not defined",
                details
            ))

        value = value.copy().set_pos(node.pos_start, node.pos_end)
        return res.success(value)

    def visit_AssignV(self, node, details):
        res = DivideError()
        var_name = node.var_name_tok.value
        value = res.register(self.visit(node.value_node, details))
        if res.error: return res

        details.symbol_table.set(var_name, value)
        return res.success(value)

    def visit_BinOpNode(self, node, details):
        res = DivideError()
        left = res.register(self.visit(node.left_node, details))
        if res.error: return res
        right = res.register(self.visit(node.right_node, details))
        if res.error: return res

        if node.op_tok.type == VAR_PLUS:
            result, error = left.added_to(right)
        elif node.op_tok.type == VAR_MINUS:
            result, error = left.subbed_by(right)
        elif node.op_tok.type == VAR_MUL:
            result, error = left.multed_by(right)
        elif node.op_tok.type == VAR_DIV:
            result, error = left.dived_by(right)
        elif node.op_tok.type == VAR_POW:
            result, error = left.powed_by(right)

        if error:
            return res.fail(error)
        else:
            return res.success(result.set_pos(node.pos_start, node.pos_end))

    def visit_UnaryOpNode(self, node, details):
        res = DivideError()
        number = res.register(self.visit(node.node, details))
        if res.error: return res

        error = None

        if node.op_tok.type == VAR_MINUS:
            number, error = number.multed_by(Number(-1))

        if error:
            return res.fail(error)
        else:
            return res.success(number.set_pos(node.pos_start, node.pos_end))


# Run Function


global_symbol_table = StoredVar()
global_symbol_table.set("null", Number(0))


def run(fn, text):
    # Generate tokens
    lexer = Lexer(fn, text)
    tokens, error = lexer.tokenizer()
    if error: return None, error

   # Goes through the syntax tree
    parser = Parser(tokens)
    tree = parser.parse()
    if tree.error: return None, tree.error

    # Program
    interpreter = Interpreter()
    details = Details('<program>')
    details.symbol_table = global_symbol_table
    result = interpreter.visit(tree.node, details)

return result.value, result.error